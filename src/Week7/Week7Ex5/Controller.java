package Week7.Week7Ex5;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

import javax.swing.*;
import java.io.*;

public class Controller {

    @FXML
    private Button save, open;
    @FXML
    private TextArea text;


    @FXML
    public void onClickMethodSave(ActionEvent actionEvent) throws IOException {

        JFileChooser c = new JFileChooser("C:\\Users\\User\\Desktop");
        int ret = c.showSaveDialog(null);
        if (ret == JFileChooser.APPROVE_OPTION) {
            try {
                String textInField = text.getText();
                StringReader sr = new StringReader(textInField);
                BufferedReader bufferedReader = new BufferedReader(sr);
                String fileNameOut = c.getSelectedFile()+".txt";
                String line = null;
                PrintWriter printLine = new PrintWriter(new FileWriter(fileNameOut, false));
                while ((line = bufferedReader.readLine()) != null) {
                    printLine.printf("%s" + "%n", line);
                }
                printLine.close();
                bufferedReader.close();
            }
            catch(IOException ex) {
                System.out.println("Error writing to file");}
        }
    }

    public void onClickMethodOpen(ActionEvent actionEvent) {
        text.clear();
        JFileChooser c = new JFileChooser("C:\\Users\\User\\Desktop");
        int ret = c.showDialog(null, "Open file...");
        String line = null;
        String fileName = null;
        if (ret == JFileChooser.APPROVE_OPTION) {
            File file = c.getSelectedFile();
            fileName = file.toString();
            try {
                FileReader fileReader = new FileReader(fileName);
                BufferedReader bufferedReader = new BufferedReader(fileReader);

                while((line = bufferedReader.readLine()) != null) {
                    text.appendText(line + "\n");
                }
                bufferedReader.close();
            }
            catch(FileNotFoundException ex) {
                System.out.println(
                        "Unable to open file '" +
                                fileName + "'");
            }
            catch(IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void clearText(ActionEvent actionEvent) {
        text.clear();
    }
}
