package smartpost;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by User on 02.12.2015.
 */
public class Controller {

    @FXML
    private Button createPackageButton;
    @FXML
    private Button removeRouteButton;
    @FXML
    private Button updatePackageButton;
    @FXML
    private Button sendPackageButton;
    @FXML
    private Button addToMapButton;
    @FXML
    private WebView mapWindow;
    @FXML
    private ComboBox placeChooser;
    @FXML
    private ComboBox classChooser;

    ObservableList<String> places = FXCollections.observableArrayList();
    SmartPostMachine spm;
    int intPlaceChoice;

    public void initialize() throws IOException{
        //showing the google map of Finland.
        mapWindow.getEngine().load(getClass().getResource("index.html").toExternalForm());
        //calling method for getting content from website and for creating object of SmartPostMachine class
        urlGetter("http://smartpost.ee/fi_apt.xml");
        for (int i = 0; i < spm.getCities().size(); i++) {
            String fullAddress = spm.getCities().get(i) + " , " + spm.getAddresses().get(i);
            places.add(fullAddress);
        }
        placeChooser.setItems(places);
        placeChooser.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                intPlaceChoice = t1.intValue();
                System.out.println(intPlaceChoice);
            }
        });


    }

    public void urlGetter(String urlName) throws IOException{
        URL url = new URL(urlName); //url with areas

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String content = "";
        String line;
        while((line = br.readLine()) != null) {
            content += line + "\n";
        }
        spm = new SmartPostMachine(content);
    }

    @FXML
    public void removeRouteAction(ActionEvent actionEvent) {
        mapWindow.getEngine().executeScript("document.deletePaths()");
    }

    @FXML
    public void updateAction(ActionEvent actionEvent) {
    }

    @FXML
    public void sendAction(ActionEvent actionEvent) {

        ArrayList<Float> al = new ArrayList<>();

        al.add(62.601125f); //lat of departureplace
        al.add(25.7236851f); //lng of departureplace
        al.add(61.0522258f); //lat of destination place
        al.add(28.1054311f); //lng of destination place

        mapWindow.getEngine().executeScript("document.createPath(" + al + ", 'red', '1')");//parameters: ArrayList[start lat, start lng, end lat, end lng], color of line, packageclass

    }

    @FXML
    public void addAction(ActionEvent actionEvent) {
        //
        mapWindow.getEngine().executeScript("document.goToLocation('Matkakatu 2, 44105, ??nekoski', 'Open: ma-pe 7.00 - 21.00, la 7.00 - 18.00, su 12.00 - 18.00', 'red')");

    }

    @FXML
    private void startWebViewAction(ActionEvent event) {

        try {
            Stage webview = new Stage();

            Parent page = FXMLLoader.load(getClass().getResource("CreatePackageWindow.fxml"));

            Scene scene = new Scene(page);

            webview.setScene(scene);
            webview.show();


        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @FXML
    private void chooseSmartPostAction(ActionEvent event) {
    }
}
