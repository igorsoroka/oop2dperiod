package smartpost;


/**
 * Created by User on 02.12.2015.
 */
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class SmartPostMachine {

    private Document doc;
    private ArrayList<String> codes;
    private ArrayList<String> cities;
    private ArrayList<String> addresses;
    private ArrayList<String> availabilities;
    private ArrayList<String> postOffices;
    private ArrayList<String> latCoordinates;
    private ArrayList<String> lngCoordinates;

    public ArrayList<String> getCodes() {
        return codes;
    }
    public ArrayList<String> getCities() {
        return cities;
    }
    public ArrayList<String> getAddresses() {
        return addresses;
    }
    public ArrayList<String> getAvailabilities() {
        return availabilities;
    }
    public ArrayList<String> getPostOffices() {
        return postOffices;
    }
    public ArrayList<String> getLatCoordinates() {
        return latCoordinates;
    }
    public ArrayList<String> getLngCoordinates() {
        return lngCoordinates;
    }

    public SmartPostMachine (String content) {
        this.codes = new ArrayList<>();
        this.cities = new ArrayList<>();
        this.addresses = new ArrayList<>();
        this.availabilities = new ArrayList<>();
        this.postOffices = new ArrayList<>();
        this.latCoordinates = new ArrayList<>();
        this.lngCoordinates = new ArrayList<>();

        try {
            //This part is for getting information from content of http://smartpost.ee/fi_apt.xml
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            doc = documentBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();

            NodeList listOfPlaces = doc.getElementsByTagName("place");
            int totalPlaces = listOfPlaces.getLength();

            for (int i = 0; i < listOfPlaces.getLength(); i++) {
                Node firstPlaceNode = listOfPlaces.item(i);
                if(firstPlaceNode.getNodeType() == Node.ELEMENT_NODE){
                    //Getting first place element
                    Element firstPlaceElement = (Element)firstPlaceNode;
                    //Getting codes from the xml file
                    NodeList codesList = firstPlaceElement.getElementsByTagName("code");
                    Element codeElement = (Element)codesList.item(0);
                    NodeList textCodeList = codeElement.getChildNodes();
                    String placeListItem = ((Node)textCodeList.item(0)).getNodeValue().trim();
                    codes.add(placeListItem);
                    //Getting names of the cities from xml
                    NodeList citiesList = firstPlaceElement.getElementsByTagName("city");
                    Element cityElement = (Element)citiesList.item(0);
                    NodeList textCityList = cityElement.getChildNodes();
                    String cityListItem = ((Node)textCityList.item(0)).getNodeValue().trim();
                    cities.add(cityListItem);
                    //Getting addresses from xml
                    NodeList addressesList = firstPlaceElement.getElementsByTagName("address");
                    Element addressElement = (Element)addressesList.item(0);
                    NodeList textAddressList = addressElement.getChildNodes();
                    String addressListItem = ((Node)textAddressList.item(0)).getNodeValue().trim();
                    addresses.add(addressListItem);
                    //Getting availability hours
                    NodeList avaList = firstPlaceElement.getElementsByTagName("availability");
                    Element avaElement = (Element)avaList.item(0);
                    NodeList textAvaList = avaElement.getChildNodes();
                    String avaListItem = ((Node)textAvaList.item(0)).getNodeValue().trim();
                    availabilities.add(avaListItem);
                    //Getting post offices
                    NodeList postList = firstPlaceElement.getElementsByTagName("postoffice");
                    Element postElement = (Element)postList.item(0);
                    NodeList textPostList = postElement.getChildNodes();
                    String postListItem = ((Node)textPostList.item(0)).getNodeValue().trim();
                    postOffices.add(postListItem);
                    //Getting coordinates of the smartpost (latitude)
                    NodeList latList = firstPlaceElement.getElementsByTagName("lat");
                    Element latElement = (Element)latList.item(0);
                    NodeList textLatList = latElement.getChildNodes();
                    String latListItem = ((Node)textLatList.item(0)).getNodeValue().trim();
                    latCoordinates.add(latListItem);
                    //Getting coordinates of the smartpost (longitude)
                    NodeList lngList = firstPlaceElement.getElementsByTagName("lng");
                    Element lngElement = (Element)lngList.item(0);
                    NodeList textLngList = lngElement.getChildNodes();
                    String lngListItem = ((Node)textLngList.item(0)).getNodeValue().trim();
                    lngCoordinates.add(lngListItem);

                    // Printing out to console for testing purposes.
                    //System.out.println(codes.get(i) + " : " + cities.get(i) + " : " + addresses.get(i) +
                    //        " : " + availabilities.get(i) + " : " + postOffices.get(i) + " : " +
                    //        latCoordinates.get(i) + " : " + lngCoordinates.get(i));
                }

            }


        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
