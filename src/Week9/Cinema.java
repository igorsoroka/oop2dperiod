package Week9;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Igor Soroka on 19.11.2015.
 */
public class Cinema {
    private Document doc;
    private Document doc2;
    private Document doc3;
    private ArrayList<String> areas;
    private ArrayList<String> ids;
    private HashMap<String, String> map;
    private ArrayList<String> dates;
    private ArrayList<String> titles;
    private ArrayList<String> times;

    public ArrayList<String> getAreas() {
        return areas;
    }

    public ArrayList<String> getIds() {
        return ids;
    }

    public ArrayList<String> getDates() {
        return dates;
    }

    public ArrayList<String> getTitles() {
        return titles;
    }

    public ArrayList<String> getTimes() {
        return times;
    }

    public String getTheatreId(int num) {
        if (map.containsKey(areas.get(num))) {
            return map.get(areas.get(num));
        }
        return null;
    }


    public Cinema(String content, String dateContent) {
        this.areas = new ArrayList<String>();
        this.ids = new ArrayList<String>();
        this.map = new HashMap<String, String>();
        this.dates = new ArrayList<>();
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = dbFactory.newDocumentBuilder();
            doc = documentBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();

                NodeList listOfTheatres = doc.getElementsByTagName("TheatreArea");
                int totalTheatreAreas = listOfTheatres.getLength();

                for (int i = 0; i < listOfTheatres.getLength(); i++) {
                    Node firstTheatreNode = listOfTheatres.item(i);
                    if(firstTheatreNode.getNodeType() == Node.ELEMENT_NODE){
                        Element firstTheatreElement = (Element)firstTheatreNode;
                        //-------
                        NodeList theatreList = firstTheatreElement.getElementsByTagName("Name");
                        Element theatreElement = (Element)theatreList.item(0);

                        NodeList textThList = theatreElement.getChildNodes();
                        String listItem = ((Node)textThList.item(0)).getNodeValue().trim();
                        areas.add(listItem);
                        //-------
                        NodeList idList = firstTheatreElement.getElementsByTagName("ID");
                        Element idElement = (Element)idList.item(0);

                        NodeList textIDList = idElement.getChildNodes();
                        String idListItem = ((Node)textIDList.item(0)).getNodeValue().trim();
                        ids.add(idListItem);
                    }
                }
                for (int i = 0; i < areas.size(); i++) { //for testing purposes
                    map.put( areas.get(i), ids.get(i) );
                }
            } catch (ParserConfigurationException e1) {
                e1.printStackTrace();
        } catch (SAXException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            DocumentBuilderFactory dbFactory2 = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder2 = dbFactory2.newDocumentBuilder();
            doc2 = documentBuilder2.parse(new InputSource(new StringReader(dateContent)));
            doc2.getDocumentElement().normalize();

            NodeList listOfDates = doc2.getElementsByTagName("dateTime");
            int totalDates = listOfDates.getLength();
            Element element = doc2.getDocumentElement();
            NodeList nodes = element.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                String date = nodes.item(i).getTextContent().trim();
                dates.add(date);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void CinemaAndShowGetter (String dateAndCinemaContent) {
        this.times = new ArrayList<>();
        this.titles = new ArrayList<>();

        try {
            DocumentBuilderFactory dbFactory3 = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder3 = dbFactory3.newDocumentBuilder();
            doc3 = documentBuilder3.parse(new InputSource(new StringReader(dateAndCinemaContent)));
            doc3.getDocumentElement().normalize();

            NodeList listOfShows = doc3.getElementsByTagName("Show");
            int totalShows = listOfShows.getLength();

            for (int i = 0; i < listOfShows.getLength(); i++) {
                Node firstShowNode = listOfShows.item(i);
                if (firstShowNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element firstTheatreElement = (Element) firstShowNode;
                    //-------
                    NodeList showList = firstTheatreElement.getElementsByTagName("OriginalTitle");
                    Element theatreElement = (Element) showList.item(0);

                    NodeList textShList = theatreElement.getChildNodes();
                    String listShItem = ((Node) textShList.item(0)).getNodeValue().trim();
                    titles.add(listShItem);
                    //--------
                    NodeList timeList = firstTheatreElement.getElementsByTagName("dttmShowStart");
                    Element timeElement = (Element) timeList.item(0);

                    NodeList textTimeList = timeElement.getChildNodes();
                    String listTimeItem = ((Node) textTimeList.item(0)).getNodeValue().trim();
                    times.add(listTimeItem);
                }
            }

        } catch (ParserConfigurationException e1) {
            e1.printStackTrace();
        } catch (SAXException e1) {
            e1.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }
}
