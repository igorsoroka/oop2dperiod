package Week7.Week7Ex1;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {

    @FXML
    private Button button;

    @FXML
    /*public void onClickMethod(){
        button.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                System.out.println("Hello World!");
            }
        });
    }*/
    public void onClickMethod(ActionEvent actionEvent) {
        System.out.println("Hello World!");
    }

}