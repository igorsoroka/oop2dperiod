package smartpost;

/**
 * Created by Igor Soroka on 06.12.2015.
 */
public abstract class Parcel {
    public double size; //size of the parcel
    public double weight; //weight of the parcel
    public double distance; //distance for travelling
    public boolean fragile; //the parcel fragile or not

    public double sizeLimit; //limit in size
    public double weightLimit; //limit in weight
    public double distanceLimit; //limit in distance
    public boolean machineBreakOrNot; ////can machine break the parcel or not

    //Setters for limits
    public abstract double setSizeLimit();
    public abstract double setWeightLimit();
    public abstract double setDistanceLimit();
    public abstract boolean setMachineBreakOrNot();

    public Parcel(double size, double weight, double distance, boolean fragile) {
        this.size = size;
        this.weight = weight;
        this.distance = distance;
        this.fragile = fragile;
        setSizeLimit();
        setWeightLimit();
        setDistanceLimit();
        setMachineBreakOrNot();
    }
    //This method is for testing purposes. To check the limits.
    public String getAllLimits() {
        return "Limit in size: " + sizeLimit + " Limit in weight: " + weightLimit + " Limit in distance: " + distanceLimit
                + " Machine breaks: " + machineBreakOrNot;
    }
    // this method is for testing purposes to check the parameters entered by user.
    public String getAllParcelParameters() {
        return "Parcel size: " + size + " Parcel weight: " + weight + " Parcel travelling distance: " + distance + " Fragile: " + fragile;
    }
}

class FirstClassParcel extends Parcel {

    public FirstClassParcel(double size, double weight, double distance, boolean fragile) {
        super(size, weight, distance, fragile);
    }

    @Override
    public double setSizeLimit() {
        return sizeLimit = 1.0;
    }

    @Override
    public double setWeightLimit() {
        return weightLimit = 1.0;
    }

    @Override
    public double setDistanceLimit() {
        return distanceLimit = 1.0;
    }

    @Override
    public boolean setMachineBreakOrNot() {
        return machineBreakOrNot = true;
    }
}

class SecondClassParcel extends Parcel {

    @Override
    public double setSizeLimit() {
        return sizeLimit = 2.0;
    }

    @Override
    public double setWeightLimit() {
        return weightLimit = 2.0;
    }

    @Override
    public double setDistanceLimit() {
        return distanceLimit = 2.0;
    }

    @Override
    public boolean setMachineBreakOrNot() {
        return machineBreakOrNot = false;
    }

    public SecondClassParcel(double size, double weight, double distance, boolean fragile) {
        super(size, weight, distance, fragile);
    }
}

class ThirdClassParcel extends Parcel {

    @Override
    public double setSizeLimit() {
        return sizeLimit = 3.0;
    }

    @Override
    public double setWeightLimit() {
        return weightLimit = 3.0;
    }

    @Override
    public double setDistanceLimit() {
        return distanceLimit = 3.0;
    }

    @Override
    public boolean setMachineBreakOrNot() {
        return machineBreakOrNot = true;
    }

    public ThirdClassParcel(double size, double weight, double distance, boolean fragile) {
        super(size, weight, distance, fragile);
    }

}