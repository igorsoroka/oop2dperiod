/*
 * Created by Olga Wolkoff
 */
package smartpost;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Olga
 */
public class CreatePackageController implements Initializable {
    @FXML
    private Label createPckageLable;
    @FXML
    private Label chooseObjectLable;
    @FXML
    private ComboBox<?> chooseObjectButton;
    @FXML
    private Label orLable;
    @FXML
    private Label creatNewObjectLable;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField massField;
    @FXML
    private RadioButton firstClassButton;
    @FXML
    private CheckBox breakableCheckBox;
    @FXML
    private RadioButton secondClassButton;
    @FXML
    private RadioButton thirdClassButton;
    @FXML
    private Button classInformationButton;
    @FXML
    private Label choseClassLable;
    @FXML
    private Label deliveryInformationLable;
    @FXML
    private Label infoTextLabel;
    @FXML
    private Button cancelButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void chooseObjectAction(ActionEvent event) {
    }

    @FXML
    private void chooseAction(ActionEvent event) {
    }

    @FXML
    private void breakableAction(ActionEvent event) {
    }

    @FXML
    private void showInfoAction(ActionEvent event) throws IOException {

        try (BufferedReader in = new BufferedReader(new FileReader("classinfo.txt"))) {
            String s;
            while((s = in.readLine()) != null){
                System.out.println(s);
                infoTextLabel.setText(infoTextLabel.getText()+"\n"+s);
            }
        }

    }


    @FXML
    private void cancelAction(ActionEvent event) {


    }

}