package Week7.Week7Ex2;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;

public class Controller {
    @FXML
    private Button button;

    @FXML
    public void onClickMethod(ActionEvent actionEvent){
        button.setText("Hello world!");
    }
}