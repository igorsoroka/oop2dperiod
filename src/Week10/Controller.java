package Week10;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.application.Application;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class Controller extends javafx.scene.layout.Region {
    @FXML
    private Button refreshBtn;
    @FXML
    private Button openBtn;
    @FXML
    private Button prevPage;
    @FXML
    private Button nextPage;
    @FXML
    WebView browser;
    WebEngine webEngine;

    String request = null;
    @FXML
    private TextField text;
    @FXML
    private Button jsBtn;

    public void initialize() throws IOException {
        if (request == null) {
            URL helloPage = getClass().getResource("index2.html");
            browser.getEngine().load(helloPage.toExternalForm());
        }

    }

    public void openPage(ActionEvent actionEvent) throws IOException {
        String cleanRequest = text.getCharacters().toString();
        if (!cleanRequest.contains("http://")) {
            request = "http://" + cleanRequest;
        }
        else
            request = cleanRequest;
        if (request.contains("index.html")) {
            URL helloPage = getClass().getResource("index2.html");
            browser.getEngine().load(helloPage.toExternalForm());
        }
        else
            browser.getEngine().load(request);


    }

    public void refreshPage(ActionEvent actionEvent) {
        browser.getEngine().load(request);
    }

    public void clickPrevPage(ActionEvent actionEvent) {
        browser.getEngine().getHistory().go(-1);
    }

    public void clickNextPage(ActionEvent actionEvent) {
        browser.getEngine().getHistory().go(1);
    }

    public void clickJs(ActionEvent actionEvent) {
        browser.getEngine().executeScript("document.shoutOut()");
    }
}