package Week9;

import com.sun.org.apache.xpath.internal.SourceTree;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Igor Soroka on 18.11.2015. Last update - 02.12.2015
 */
//http://www.finnkino.fi/xml
//www.finnkino.fi/xml/TheatreAreas/ - this is the chooser of theatres
//www.finnkino.fi/xml/ScheduleDates/ schedule dates
//https://www.youtube.com/watch?v=-y15EfjRLlU - Lecture about usage of XML in Java

public class Controller {
    public TextArea textArea;

    @FXML
    ChoiceBox movieChooser;

    @FXML
    TextField showDate;

    @FXML
    TextField beginTime;

    @FXML
    TextField endTime;

    @FXML
    TextField movieTitle;

    @FXML
    Button searchMovieByName;



    ArrayList<String> cinemas;
    Cinema cinema;
    String idURL = null;
    String movieAndDateURL = null;
    String dateURL = null;
    String year = null;
    String month = null;
    String day = null;
    SimpleDateFormat formatter;

    ObservableList<String> areas = FXCollections.observableArrayList();
    ArrayList<String> dates = new ArrayList<>();
    String theatreDateUrl = null;
    Map<Date, String> dateAndTimeSpecificCinema= new HashMap<>();

    @FXML
    public void initialize() throws IOException {
        //initialized here because it can be used for getting specific content of shows start in particular theatre
        urlGetter("http://www.finnkino.fi/xml/TheatreAreas/", "http://www.finnkino.fi/xml/ScheduleDates/");
        for (int j = 0; j < cinema.getAreas().size(); j++) {
            areas.add(cinema.getAreas().get(j));
        }
        movieChooser.setItems(areas);
        movieChooser.setValue(areas.get(0));
        //ListView with listener
        movieChooser.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                idURL = cinema.getTheatreId(t1.intValue());
                }
        });
        //set textarea read-only
        textArea.setEditable(false);
        //Date format for using formatter.parser()
        String expectedPattern = "yyyy-MM-dd'T'HH:mm:ss";
        formatter = new SimpleDateFormat(expectedPattern);

    }

    //Action for pressing Search By Name
    public void pressSearchByNameBtn(ActionEvent actionEvent) throws IOException, ParseException {
        textArea.clear();
        String unformatDate = showDate.getCharacters().toString();
        /*//movieAndDateURL = "http://www.finnkino.fi/xml/Schedule/?area=" + idURL + "&dt=" + returnDate(unformatDate);
        //urlGetterCinemaAndShows(movieAndDateURL);
        //for (int j = 0; j < cinema.getTitles().size(); j++) {
        //    if (cinema.getTitles().get(j).contains(movieTitle.getCharacters().toString())) {
        //        textArea.appendText(cinema.getTitles().get(j) + " at " + formatter.parse(cinema.getTimes().get(j)).toString() + "\n");
        //    }
        //}*/
        for (int i = 1; i < cinema.getAreas().size(); i++) {
            movieAndDateURL = "http://www.finnkino.fi/xml/Schedule/?area=" + cinema.getIds().get(i) + "&dt=" + returnDate(unformatDate);
            urlGetterCinemaAndShows(movieAndDateURL);
            for (int j = 0; j < cinema.getTitles().size(); j++) {
                if (cinema.getTitles().get(j).contains(movieTitle.getCharacters().toString())) {
                    textArea.appendText(cinema.getTitles().get(j) + " at " + formatter.parse(cinema.getTimes().get(j)).toString() +
                            " in " + cinema.getAreas().get(i) + "\n");
                }
            }
        }

    }

    @FXML
    //Action for pressing Show list of movies
    public void pressShowBtn(ActionEvent actionEvent) throws IOException, ParseException {
            textArea.clear();
            movieTitle.clear();
            //This condition for showing the full timetable for a day
            if (beginTime.getCharacters().toString().isEmpty() && endTime.getCharacters().toString().isEmpty() //show all day movies
                    && movieTitle.getCharacters().toString().isEmpty()) {
                String unformatDate = showDate.getCharacters().toString();
                movieAndDateURL = "http://www.finnkino.fi/xml/Schedule/?area=" + idURL + "&dt=" + returnDate(unformatDate);
                urlGetterCinemaAndShows(movieAndDateURL);
                for (int k = 0; k < cinema.getTitles().size(); k++) {
                    textArea.appendText(cinema.getTitles().get(k) + " at " + formatter.parse(cinema.getTimes().get(k)).toString() + "\n");
                }
            }
            //Will work only with full interval of time
            else {
                try {
                    String startTime = year + "-" + month + "-" + day + "T" + beginTime.getCharacters().toString() + ":00";
                    String endingTime = year + "-" + month + "-" + day + "T" + endTime.getCharacters().toString() + ":00";
                    Date dateBegin = formatter.parse(startTime);
                    Date dateEnd = formatter.parse(endingTime);
                    ArrayList<Date> datesFormatted = new ArrayList<>();
                    for (int l = 0; l < cinema.getTimes().size(); l++) {
                        datesFormatted.add(formatter.parse(cinema.getTimes().get(l)));
                        dateAndTimeSpecificCinema.put(formatter.parse(cinema.getTimes().get(l)), cinema.getTitles().get(l));
                        if (dateBegin.compareTo(datesFormatted.get(l)) <= 0 && dateEnd.compareTo(datesFormatted.get(l)) >= 0) {
                            textArea.appendText(dateAndTimeSpecificCinema.get(datesFormatted.get(l))
                                    + " at " + datesFormatted.get(l) + "\n");
                        }
                    }
                }
                catch(ParseException e) {
                    e.printStackTrace();
                }
            }
        }


    public void urlGetter(String urlName, String urlName2) throws IOException { //method for finding information in specific URL
        URL url = new URL(urlName); //url with areas
        URL url2 = new URL(urlName2); //url with dates
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String content = "";
        String line;
        while((line = br.readLine()) != null) {
            content += line + "\n";
        }
        BufferedReader br2 = new BufferedReader(new InputStreamReader(url2.openStream()));
        String dateContent = "";
        String line2;
        while((line2 = br2.readLine()) != null) {
            dateContent += line2 + "\n";
        }

        cinema = new Cinema(content, dateContent);
    }
    public void urlGetterCinemaAndShows(String urlName3) throws IOException { //method for searching through the specific day in specific cinema
        URL url3 = new URL(urlName3);
        BufferedReader br3 = new BufferedReader(new InputStreamReader(url3.openStream()));
        String dateAndCinemaContent = "";
        String line3;
        while((line3 = br3.readLine()) != null) {
            dateAndCinemaContent += line3 + "\n";
        }
        cinema.CinemaAndShowGetter(dateAndCinemaContent);
    }

    public String returnDate(String unformatDate) {
        for (int k = 1; k < cinema.getDates().size(); k+=2) {
            year = cinema.getDates().get(k).substring(0, 4);
            month = cinema.getDates().get(k).substring(5, 7);
            day = cinema.getDates().get(k).substring(8, 10);
            String dmy = day + "." + month + "." + year;
            String dmy2 = day + "/" + month + "/" + year;
            String dmy3 = day + month + year;
            if (unformatDate.equals(dmy) || unformatDate.equals(dmy2) || unformatDate.equals(dmy3)) {
                dateURL = dmy;
                break;
            }
        }
        return dateURL;
    }
}
