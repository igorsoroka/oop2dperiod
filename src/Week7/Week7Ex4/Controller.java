package Week7.Week7Ex4;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

public class Controller {

    @FXML
    private TextField textfield;

    @FXML
    public void enterPressed() {
        textfield.clear();
        textfield.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent ke) {
            if (ke.getCode().equals(KeyCode.ENTER)) {
                System.out.println(textfield.getCharacters());
                }
            }
        });
}
}