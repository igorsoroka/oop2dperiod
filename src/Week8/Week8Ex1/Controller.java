package Week8.Week8Ex1;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Duration;
import sun.util.calendar.LocalGregorianCalendar;

import java.awt.event.MouseEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

public class Controller {

    ObservableList<Double> coins = FXCollections.observableArrayList(0.10,
            0.20, 0.50, 1.0, 2.0);
    ObservableList<String> manArray = FXCollections.observableArrayList("Pepsi Max", "Coca-Cola Zero", "Fanta Zero");
    ObservableList<String> sizes = FXCollections.observableArrayList("0.5", "1.5");

    @FXML
    private CheckBox check1;
    @FXML
    private CheckBox check2;
    @FXML
    private CheckBox check3;
    @FXML
    private CheckBox check4;
    @FXML
    private CheckBox check5;
    @FXML
    private CheckBox check6;
    @FXML
    private ProgressBar progress;
    @FXML
    private ChoiceBox choiceBox1;
    @FXML
    private ChoiceBox choiceBox2;
    @FXML
    private TextField commandLine;
    @FXML
    private TextField prepared;
    @FXML
    private Button reset;
    @FXML
    private Button add;
    @FXML
    private Button payBtn;
    @FXML
    private Slider slider;

    String chosenManufacturer = null;
    String chosenVolume = null;
    double addedMoney;
    double moneyPaid;
    double returnedMoney;
    double chosenBottlePrice;
    int choice; //for remembering the choice in the buying of lemonades

    @FXML
    public void initialize() { //initialization of choiceboxes, checkboxes and slider
        commandLine.setEditable(false); //do not allow user to edit the textfield
        prepared.setEditable(false);
        commandLine.setText("Hello! Please add money to the soda machine...");

        choiceBox1.setItems(manArray);
            choiceBox1.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                    if (t1.intValue() != -1) {
                        chosenManufacturer = manArray.get(t1.intValue());
                        check2.setSelected(true);
                    }
                }
            });

            choiceBox2.setItems(sizes);
            choiceBox2.getSelectionModel().selectedIndexProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                    if (t1.intValue() != -1) {

                        chosenVolume = sizes.get(t1.intValue());
                        try {
                            for (int i = 0; i < BottleDispenser.getInstance().bottlesArray.length; i++) {
                                if ((BottleDispenser.getInstance().bottlesArray[i].getName().contains(chosenManufacturer))
                                        && (BottleDispenser.getInstance().bottlesArray[i].getSize().contains(chosenVolume))) {
                                    chosenBottlePrice = BottleDispenser.getInstance().bottlesArray[i].getPrice();
                                }
                            }
                            commandLine.setText("Your choice: " + chosenManufacturer + " " + chosenVolume + "L."
                                    + " Price is " + chosenBottlePrice + "\u20ac");
                            check3.setSelected(true);
                        }
                        catch (NullPointerException e) {}
                    }
                }
            });

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                                Number oldValue, Number newValue) {
                addedMoney = newValue.doubleValue();
            }
        });

    }
    @FXML
    public void onClickAddMethod(ActionEvent actionEvent) { //ADD button is pressed
        BottleDispenser.getInstance().setMoney(addedMoney);
        commandLine.setText("Klink! Coin added! Your account: " + BottleDispenser.getInstance().getMoney() + "\u20ac");
        check1.setSelected(true);
    }

    public void clickPayBtn(ActionEvent actionEvent) throws Exception { // PAY button is pressed
        try {
            if (!check6.isSelected()) {
                for (int i = 0; i < BottleDispenser.getInstance().bottlesArray.length; i++) {
                    if ((BottleDispenser.getInstance().bottlesArray[i].getName().contains(chosenManufacturer))
                            && (BottleDispenser.getInstance().bottlesArray[i].getSize().contains(chosenVolume))
                            && (BottleDispenser.getInstance().bottlesArray[i].getPrice() == chosenBottlePrice)) {
                        choice = i;
                    }
                }
                progress.setProgress(1.0);
                check4.setSelected(true);
                moneyPaid = BottleDispenser.getInstance().getMoney();
                prepared.setText(BottleDispenser.getInstance().buyBottle(choice));
                BottleDispenser.getInstance().removeBottle(choice);
                check5.setSelected(true);
                if (check4.isSelected() && check5.isSelected()) {
                    returnedMoney = BottleDispenser.getInstance().getMoney();
                    commandLine.setText(BottleDispenser.getInstance().returnMoney());
                    receiptPrint();
                }
                check6.setSelected(true);
            }
        }
        catch (NullPointerException e) { //catching NULL in the BottlesArray! ! !
            commandLine.setText("No this beverage. Choose another. Press RESET...");
        }
    }

    public void resetAction(ActionEvent actionEvent) { //RESET button is pressed
        commandLine.setText("Your return is " + BottleDispenser.getInstance().getMoney() + "\u20ac");
        BottleDispenser.getInstance().money = 0.0;
    }

    public void receiptPrint() throws Exception { //method for printing the receipt in file
        ArrayList<String> receiptList = new ArrayList<>();
        receiptList.add("\t\tSoroka Soda Machine");
        receiptList.add("\t34 Skinnarilankatu, FI-53850, Lappeenranta");
        Date today = new Date();
        receiptList.add(" ");
        receiptList.add(today.toString());
        receiptList.add("--------------------------");
        receiptList.add("1 " + chosenManufacturer + " " + chosenVolume + "L" + "\t " + chosenBottlePrice + " \u20ac");
        receiptList.add("--------------------------");
        receiptList.add("AMOUNT PAID: " + moneyPaid);
        receiptList.add("RETURN: " + returnedMoney);
        receiptList.add(" ");
        receiptList.add("\t\tThank you!");
        PrintWriter pw = new PrintWriter(new FileOutputStream("C:\\Users\\User\\Desktop\\receipt.txt"));
            for (String l : receiptList)
                pw.println(l);
            pw.close();
        }

    public void startAgain(Event event) { //restarting program
        if (check1.isSelected() && check2.isSelected() && check3.isSelected()
                && check4.isSelected() && check5.isSelected() && check6.isSelected()) {
            commandLine.clear();
            prepared.clear();
            progress.setProgress(0.0);
            BottleDispenser.getInstance().money = 0.0;
            check1.setSelected(false);
            check2.setSelected(false);
            check3.setSelected(false);
            check4.setSelected(false);
            check5.setSelected(false);
            check6.setSelected(false);
            choiceBox1.getSelectionModel().clearSelection();
            choiceBox2.getSelectionModel().clearSelection();
            commandLine.setText("Hello! Please add money to the soda machine...");
        }
    }
}
