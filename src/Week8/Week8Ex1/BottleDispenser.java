package Week8.Week8Ex1;

import java.util.ArrayList;

/**
 * Created by User on 09.11.2015.
 */
public class BottleDispenser {
    public int bottles;
    public double money;
    private static final BottleDispenser INSTANCE =
            new BottleDispenser();
    Bottle[] bottlesArray = new Bottle[6];

    private BottleDispenser() {
        bottles = 6;
        money = 0.0;
        CreateBottleList();
    }
    public static final BottleDispenser getInstance() {
        return INSTANCE;
    }
    public void CreateBottleList() {
            bottlesArray[0] = new Bottle(0, 0, 0);
            bottlesArray[1] = new Bottle(0, 1, 1);
            bottlesArray[2] = new Bottle(1, 0, 2);
            bottlesArray[3] = new Bottle(1, 1, 3);
            bottlesArray[4] = new Bottle(2, 0, 4);
            bottlesArray[5] = new Bottle(2, 0, 5);
    }
    public String buyBottle(int num) {
        if (money <= 0) {
            money = 0;
            return "Enter money first";
        }
        else if (money < bottlesArray[num].getPrice())
            return "Enter money first";
        else {
            money = money - bottlesArray[num].getPrice();
            bottles = bottles - 1;
            return "KRACHUNK! " + bottlesArray[num].getName() + " came!";
        }
    }
    public String returnMoney() {
        return "Klink-klink! Your return is " + money + "\u20ac";
    }
    public void removeBottle(int place) {
        bottlesArray[place] = null;
    }


    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money += money;
    }
}
class Bottle {
    private String name, size;
    private double price;
    public String[] manArray = new String [] {"Pepsi Max", "Coca-Cola Zero", "Fanta Zero"};
    public String[] sizeArray = new String[]{"0.5", "1.5"};
    public double[] priceArray = new double[]{1.8, 2.2, 2.0, 2.5, 1.95, 2.65};

    Bottle (int manElement, int sizeElement, int priceElement) {
        this.name = manArray[manElement];
        this.size = sizeArray[sizeElement];
        this.price = priceArray[priceElement];
    }

    public String getName() {
        return this.name;
    }

    public double getPrice() {
        return this.price;
    }

    public String getSize() {
        return this.size;
    }

}

