package Week7.Week7Ex3;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private Button button;
    @FXML
    private TextField tf;

    public void onClickMethod(ActionEvent actionEvent) {
        System.out.println(tf.getCharacters());
    }
}